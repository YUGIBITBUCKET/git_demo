# Getting started with `git`

## Introduction
This **git repository** is created as a demo remote repo for the VLSI 2021 Q2 batch of freshers. Refer online for more markdown syntax.

## Creating a Local Repository: Command List

- `git init` 
- `git config --local user.name YOUR_NAME`
- `git config --local user.email EMAIL`
- add / change some files
- `git status -s` or `git status`
- `git add FILENAME(S)`
- `git commit -m "COMMIT MSG DESCRIBING CHANGES"`
- `git log --oneline`

You have created a local repo